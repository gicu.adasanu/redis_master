FROM openjdk:latest

ADD target/redis_master-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar" ]

EXPOSE 7979
