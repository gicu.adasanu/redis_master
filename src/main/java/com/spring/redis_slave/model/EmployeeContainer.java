package com.spring.redis_slave.model;

import java.io.Serializable;

public class EmployeeContainer implements Serializable {
    Employee employee;
    String dbUri;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getDbUri() {
        return dbUri;
    }

    public void setDbUri(String dbUri) {
        this.dbUri = dbUri;
    }

    public EmployeeContainer(Employee employee, String dbUri) {
        this.employee = employee;
        this.dbUri = dbUri;
    }
}
