package com.spring.redis_slave.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.List;

public class Employee implements Serializable {

    private String id;
    private String name;
    private String surname;
    private String post;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Employee() {}

    public Employee(String id, String name, String surname, String post) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.post = post;
    }

    public static Employee fromJson(String employeeJson) {
        Gson gson = new Gson();
        return gson.fromJson(employeeJson, Employee.class);
    }

    public static List<Employee> fromJsonToList(String employeeJson) {
        Gson gson = new Gson();
        return gson.fromJson(employeeJson, new TypeToken<List<Employee>>(){}.getType());
    }
}
