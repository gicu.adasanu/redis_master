package com.spring.redis_slave.service;

import com.spring.redis_slave.exception.NoContentException;
import com.spring.redis_slave.model.Employee;
import com.spring.redis_slave.model.EmployeeContainer;
import com.spring.redis_slave.repository.RedisRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
public class RedisService {
    private final RedisRepository redisRepository;

//    public List<Employee> getAllEmployee() {
//        List<Employee> employeeList = redisRepository.getAll();
//
//        if (!employeeList.isEmpty()) {
//            return employeeList;
//        }
//
//        throw new NoContentException();
//    }

    public void createEmployee(String name, String surname, String value, String dbUri) {
        Employee employee = Employee.fromJson(value);
        EmployeeContainer employeeContainer = new EmployeeContainer(employee, dbUri);
        redisRepository.put(name + "_" + surname, employeeContainer);
    }

    public void updateEmployee(String name, String surname, String newName, String newSurname, String value, String dbUri) {
        redisRepository.delete(name + "_" + surname);

        Employee employee = Employee.fromJson(value);
        EmployeeContainer employeeContainer = new EmployeeContainer(employee, dbUri);
        redisRepository.put(newName + "_" + newSurname, employeeContainer);
    }

    public void deleteEmployee(String name, String surname) {
        redisRepository.delete(name + "_" + surname);
    }
}
