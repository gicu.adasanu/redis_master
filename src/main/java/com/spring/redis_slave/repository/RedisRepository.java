package com.spring.redis_slave.repository;

import com.spring.redis_slave.model.Employee;
import com.spring.redis_slave.model.EmployeeContainer;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class RedisRepository {
    private HashOperations hashOperations;
    private RedisTemplate redisTemplate;

    public RedisRepository(RedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
        this.hashOperations = this.redisTemplate.opsForHash();
    }

    public boolean containsKey(String key) {
        return hashOperations.hasKey("EMPLOYEE", key);
    }

    public void put(String key, EmployeeContainer value) {
        hashOperations.put("EMPLOYEE", key, value);
    }

    public void delete(String key) {
        hashOperations.delete("EMPLOYEE", key);
    }

}
