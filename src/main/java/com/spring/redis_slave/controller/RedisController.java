package com.spring.redis_slave.controller;

import com.spring.redis_slave.model.Employee;
import com.spring.redis_slave.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/employee")
public class RedisController {
    @Autowired
    RedisService redisService;

    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public void createEmployee(@RequestParam(name = "name") String name,
                               @RequestParam(name = "surname") String surname,
                               @RequestParam(name = "value") String value,
                               @RequestParam(name = "dbUri") String dbUri) {

        redisService.createEmployee(name, surname, value, dbUri);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void updateEmployee(@RequestParam(name = "name") String name,
                               @RequestParam(name = "surname") String surname,
                               @RequestParam(name = "newName") String newName,
                               @RequestParam(name = "newSurname") String newSurname,
                               @RequestParam(name = "value") String value,
                               @RequestParam(name = "dbUri") String dbUri) {

        redisService.updateEmployee(name, surname, newName, newSurname, value, dbUri);
    }


    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deleteEmployee(@ModelAttribute(name = "name") String name,
                               @ModelAttribute(name = "surname") String surname) {

        redisService.deleteEmployee(name, surname);
    }
}
