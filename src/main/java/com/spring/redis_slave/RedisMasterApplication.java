package com.spring.redis_slave;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisMasterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisMasterApplication.class, args);
    }

}
