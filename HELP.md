1. cd in path of project
2. run mvn package
3. edit if necessary modify the configuration of docker (Docker file)
4. create image docker : docker build -t redismasterslave .
5. check if exist docker image with command: docker image ls
6. run app: docker run -p 7999:7999 redismasterslave

